﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class ProdutoController : Controller
    {
        public static ProdutoViewModel objeto = new ProdutoViewModel();

        // GET: Produto
        public ActionResult Index()
        {
            return View(objeto);
        }

        // GET: Produto/Details
        public ActionResult Details(int codigo)
        {
            var obj = new ProdutoViewModel();

            for (int i = 0; i < objeto.listaUsuarios.Count; i++)
            {
                if (codigo == objeto.listaUsuarios[i].Codigo)
                {
                    obj.Codigo = objeto.listaUsuarios[i].Codigo;
                    obj.Nome = objeto.listaUsuarios[i].Nome;
                    obj.DataDeCadastro = objeto.listaUsuarios[i].DataDeCadastro;
                    obj.cor = objeto.listaUsuarios[i].cor;
                    obj.Fornecedor = objeto.listaUsuarios[i].Fornecedor;
                    obj.Preco = objeto.listaUsuarios[i].Preco;

                    break;
                }
            }

            return View(obj);
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            var obj = new ProdutoViewModel();

            return View(obj);
        }

        // POST: Produto/Create
        [HttpPost]
        public ActionResult Create(ProdutoViewModel produto)
        {
            objeto.listaUsuarios.Add(produto);



            ViewBag.Mensagem = true;

            return RedirectToAction("Index");
        }

        // GET: Produto/Edit
        public ActionResult Edit(int codigo)
        {
            var obj = new ProdutoViewModel();

            for (int i = 0; i < objeto.listaUsuarios.Count; i++)
            {
                if (codigo == objeto.listaUsuarios[i].Codigo)
                {
                    obj.Nome = objeto.listaUsuarios[i].Nome;
                    obj.DataDeCadastro = objeto.listaUsuarios[i].DataDeCadastro;
                    obj.cor = objeto.listaUsuarios[i].cor;
                    obj.Fornecedor = objeto.listaUsuarios[i].Fornecedor;
                    obj.Preco = objeto.listaUsuarios[i].Preco;

                    break;
                }
            }

            return View(obj);
        }

        // POST: Produto/Edit
        [HttpPost]
        public ActionResult Edit(ProdutoViewModel produto)
        {
            for (int i = 0; i < objeto.listaUsuarios.Count; i++)
            {
                if (produto.Codigo == objeto.listaUsuarios[i].Codigo)
                {
                    objeto.listaUsuarios[i].Nome = produto.Nome;
                    objeto.listaUsuarios[i].DataDeCadastro = produto.DataDeCadastro;
                    objeto.listaUsuarios[i].cor = produto.cor;
                    objeto.listaUsuarios[i].Fornecedor = produto.Fornecedor;
                    objeto.listaUsuarios[i].Preco = produto.Preco;
                }
            }

            return RedirectToAction("Index");
        }

        // GET: Produto/Delete
        public ActionResult Delete(int codigo)
        {
            var obj = new ProdutoViewModel();

            for (int i = 0; i < objeto.listaUsuarios.Count; i++)
            {
                if (codigo == objeto.listaUsuarios[i].Codigo)
                {
                    obj.Nome = objeto.listaUsuarios[i].Nome;
                    obj.DataDeCadastro = objeto.listaUsuarios[i].DataDeCadastro;
                    obj.cor = objeto.listaUsuarios[i].cor;
                    obj.Fornecedor = objeto.listaUsuarios[i].Fornecedor;
                    obj.Preco = objeto.listaUsuarios[i].Preco;

                    break;
                }
            }

            return View(obj);
        }

        // POST: Produto/Delete
        [HttpPost]
        public ActionResult Delete(ProdutoViewModel produto)
        {
            for (int i = 0; i < objeto.listaUsuarios.Count; i++)
            {
                if (produto.Codigo == objeto.listaUsuarios[i].Codigo)
                {
                    objeto.listaUsuarios.RemoveAt(i);
                }
            }

            return RedirectToAction("Index");
        }
    }
}