﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class ProdutoViewModel
    {
        [Required]
        [Display(Name = "Nome do produto")]
        [StringLength(10)]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data de Cadastro")]
        public string DataDeCadastro { get; set; }

        [Required]
        public int Codigo { get; set; }

        [Required]
        [StringLength(10)]
        public string Fornecedor { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Preco { get; set; }

        [Required]
        [StringLength(10)]
        public string cor { get; set; }

        public List<ProdutoViewModel> listaUsuarios = new List<ProdutoViewModel>();
    }
}